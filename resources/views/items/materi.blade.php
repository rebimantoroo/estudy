@extends('adminlte.master')

@section('judulFile')
  Halaman Materi
@endsection

@section('judul1')
  Materi
@endsection

@section('judul2')
    Web Developer
@endsection

@section('isi')
    <div class = "row">
      <div class = "col">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">HTML Dasar</h5>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="/detailMateri1" class="btn btn-success">Go somewhere</a>
          </div>
        </div>
      </div>  

      <div class = "col">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">CSS Dasar</h5>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="/detailMateri2" class="btn btn-success">Go somewhere</a>
          </div>
        </div>
      </div>  


      <div class = "col">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">CSS Layouting</h5>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="/detailMateri3" class="btn btn-success">Go somewhere</a>
          </div>
        </div>
      </div>  
      
      <div class = "col">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Dasar Pemrograman Javascript</h5>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="/detailMateri4" class="btn btn-success">Go somewhere</a>
          </div>
        </div>
      </div>  

      <div class = "col">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">PHP Dasar</h5>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="/detailMateri5" class="btn btn-success">Go somewhere</a>
          </div>
        </div>
      </div>  


      <div class = "col">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Javascript DOM</h5>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="/detailMateri6" class="btn btn-success">Go somewhere</a>
          </div>
        </div>
      </div> 


    </div>

@endsection


