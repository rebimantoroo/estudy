@extends('adminlte.master')

@section('judulFile')
  Halaman Latihan
@endsection

@section('judul1')
  Latihan
@endsection

@section('judul2')
  Studi Kasus Software Developer
@endsection

@section('isi')
    <div class = "row">
      <div class = "col-4">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Software Developer</h5>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="#" class="btn btn-success">Go somewhere</a>
          </div>
        </div>
      </div>  

      <div class = "col-4">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">UI/UX Designer</h5>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="#" class="btn btn-success">Go somewhere</a>
          </div>
        </div>
      </div>  


      <div class = "col-4">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Techno Preneur</h5>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="#" class="btn btn-success">Go somewhere</a>
          </div>
        </div>
      </div>  


    </div>

</div>
@endsection

@section('isi2')
<div class="card mt-5">
        <div class="card-header">
          <h1 class="card-title">Study Kasus UI UX Designer</h1>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body">
        <div class = "row">
      <div class = "col-4">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Software Developer</h5>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="#" class="btn btn-success">Go somewhere</a>
          </div>
        </div>
      </div>  

      <div class = "col-4">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">UI/UX Designer</h5>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="#" class="btn btn-success">Go somewhere</a>
          </div>
        </div>
      </div>  


      <div class = "col-4">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Techno Preneur</h5>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="#" class="btn btn-success">Go somewhere</a>
          </div>
        </div>
      </div>  


    </div>

</div>
        </div>
@endsection

@section('isi3')
<div class="card mt-5">
        <div class="card-header">
          <h1 class="card-title">Study Kasus Techno Preneur</h1>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body">
        <div class = "row">
      <div class = "col-4">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Software Developer</h5>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="#" class="btn btn-success">Go somewhere</a>
          </div>
        </div>
      </div>  

      <div class = "col-4">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">UI/UX Designer</h5>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="#" class="btn btn-success">Go somewhere</a>
          </div>
        </div>
      </div>  


      <div class = "col-4">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Techno Preneur</h5>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="#" class="btn btn-success">Go somewhere</a>
          </div>
        </div>
      </div>  


    </div>

</div>
        </div>
@endsection