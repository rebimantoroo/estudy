@extends('adminlte.master')

@section('judulFile')
  Halaman Selamat Datang
@endsection

@section('judul1')
  Selamat datang , Bima!
@endsection

@section('judul2')
  Show Off Project ESD Laboratory
@endsection


@section('isi')
    <div class = "row">
      <div class = "col-4">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Software Developer</h5>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="/show1" class="btn btn-success">Go somewhere</a>
          </div>
        </div>
      </div>  

      <div class = "col-4">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">UI/UX Designer</h5>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="/show2" class="btn btn-success">Go somewhere</a>
          </div>
        </div>
      </div>  


      <div class = "col-4">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Techno Preneur</h5>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="show3" class="btn btn-success">Go somewhere</a>
          </div>
        </div>
      </div>  


    </div>

</div>
@endsection
